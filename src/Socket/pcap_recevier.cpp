#include "pcap_recevier.h"

namespace noname_core {

	pcap_recevier::pcap_recevier()
		: socket_receiver()
		, handle(nullptr) { }

	bool pcap_recevier::connect(const std::string dev_name = "default")
	{
		handle = pcap_open_live(dev_name.c_str(), BUFSIZ, 1, 1000, errbuf);
		if (handle == NULL) {
			std::cerr << "Couldn't open device " << dev_name << errbuf << std::endl;
			return false;
		}
		return true;
	}

	packet pcap_recevier::receive()
	{
		struct pcap_pkthdr* p_header;
		const u_char* packet_data;

		pcap_next_ex(handle, &p_header, &packet_data);
		return packet(reinterpret_cast<uint8_t*>(const_cast<u_char*>(packet_data))
			, static_cast<std::size_t>(p_header->len));
	}

	void pcap_recevier::start()
	{
	}

	void pcap_recevier::stop()
	{
	}

	pcap_t* pcap_recevier::get_handle() const
	{
		return handle ? handle : nullptr;
	}


}