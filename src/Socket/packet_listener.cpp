#include "packet_listener.h"

namespace noname_core {
	packet_listener::packet_listener(std::string identifier)
		: identifier(identifier)
		, callback(nullptr) { }

	packet_listener::packet_listener(std::string identifier, std::function<void(packet)>& callback)
		: identifier(identifier)
		, callback(callback) { }

	void packet_listener::update(packet& packet)
	{
		if (callback)
			callback(packet);
	}

	void packet_listener::set_callback(std::function<void(packet)>& callback)
	{
		this->callback = callback;
	}

	void packet_listener::release_callback()
	{
		this->callback = nullptr;
	}

	bool packet_listener::operator==(const packet_listener& pl)
	{
		return identifier == pl.identifier;
	}

}