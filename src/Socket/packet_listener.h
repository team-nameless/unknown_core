#pragma once

#include "..//Network/packet.h"

#include <functional>

namespace noname_core {
	class packet_listener
	{
	public:
		packet_listener(std::string identifier);
		packet_listener(std::string identifier, std::function<void(packet)>& callback);
		virtual void update(packet& packet);
		virtual void set_callback(std::function<void(packet)>& callback);
		virtual void release_callback();

		bool operator==(const packet_listener& pl);
	private:
		std::function<void(packet)> callback;
		std::string identifier;
	};
}