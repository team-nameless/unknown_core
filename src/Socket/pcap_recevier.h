#pragma once

#include "socket_receiver.h"

#include "Include//pcap.h"

namespace noname_core {
	class pcap_recevier :
		public socket_receiver
	{
	public :
		pcap_recevier();

		bool connect(const std::string dev_name = "default") override;
		packet receive() override;
		void start() override;
		void stop() override;

		pcap_t* get_handle() const;
	private:
		pcap_t* handle;
		char errbuf[PCAP_ERRBUF_SIZE];
	};
}
