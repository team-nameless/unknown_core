#include "socket_receiver.h"

namespace noname_core {

	socket_receiver::socket_receiver() { }

	void socket_receiver::add_listener(packet_listener& pl)
	{
		listeners.push_back(pl);
	}

	void socket_receiver::delete_listener(packet_listener& pl)
	{
		for (auto it = listeners.begin(); it != listeners.end(); ++it)
		{
			if (*it == pl)
			{
				listeners.erase(it);
				break;
			}
		}
	}
	void socket_receiver::notify(packet& p)
	{
		for (auto li : listeners)
		{
			li.update(p);
		}
	}
}
