#pragma once

#include "..//Network/packet.h"
#include "packet_listener.h"

#include <string>
#include <vector>

namespace noname_core {
	class socket_receiver {
	public:
		socket_receiver();

		virtual bool connect(const std::string dev_name = "default") = 0;
		virtual packet receive() = 0;
		virtual void start() = 0;
		virtual void stop() = 0;
		
		virtual void add_listener(packet_listener& pl);
		virtual void delete_listener(packet_listener& pl);
		virtual void notify(packet& p);
	private:
		std::vector<packet_listener> listeners;
	};
}