#pragma once

#include "network.hpp"
#include "header.hpp"
#include "ethernet.hpp"
#include "ipv4.hpp"

namespace noname_core {
	struct arp_header final : public header<arp_header> {
		static constexpr auto ARP_REQUEST = 1;
		static constexpr auto ARP_REPLY = 2;
		static constexpr auto RARP_REQUEST = 3;
		static constexpr auto RARP_REPLY = 4;

	private:
		uint16_t hardware_type;
		uint16_t proto_type;

		uint8_t hardware_size;
		uint8_t proto_size;

		uint16_t opcode;

		mac_address sender_mac;
		ip_address sender_ip;

		mac_address target_mac;
		ip_address target_ip;

	public:
		arp_header();
		arp_header(const uint8_t* data);
		arp_header(const arp_header& a);

		uint16_t get_hardware_type() const;
		uint16_t get_proto_type() const;
		uint8_t get_hardware_size() const;
		uint8_t et_proto_size() const;
		uint16_t get_opcode() const;
		mac_address get_sender_mac() const;
		ip_address get_sender_ip() const;
		mac_address get_target_mac() const;
		ip_address get_target_ip() const;

		inline std::string to_string() const;

		arp_header  operator+(const arp_header& a) const = delete;
		arp_header  operator-(const arp_header& a) const = delete;
		arp_header  operator*(const arp_header& a) const = delete;
		arp_header  operator/(const arp_header& a) const = delete;
		arp_header  operator%(const arp_header& a) const = delete;
		arp_header& operator=(const arp_header& a);
		bool		 operator==(const arp_header& a) const;
		bool		 operator!=(const arp_header& a) const;

		friend std::ostream& operator<<(std::ostream& os, const arp_header& a);
	};

	inline arp_header::arp_header()
		: hardware_type(bswap16(1))
		, proto_type(bswap16(ethernet_header::ETHER_TYPE_IP))
		, hardware_size(6)
		, proto_size(4)
		, opcode(0)
		, sender_mac(0)
		, sender_ip(0)
		, target_mac(0)
		, target_ip(0) { }

	inline arp_header::arp_header(const uint8_t* data)
		: hardware_type(data[0])
		, proto_type(data[2])
		, hardware_size(data[4])
		, proto_size(data[5])
		, opcode(data[6])
		, sender_mac(data + 8)
		, sender_ip(data + 14)
		, target_mac(data + 18)
		, target_ip(data + 24) { }

	inline arp_header::arp_header(const arp_header& a)
		: hardware_type(a.hardware_type)
		, proto_type(a.proto_type)
		, hardware_size(a.hardware_size)
		, proto_size(a.proto_size)
		, opcode(a.opcode)
		, sender_mac(a.sender_mac)
		, sender_ip(a.sender_ip)
		, target_mac(a.target_mac)
		, target_ip(a.target_ip) { }

	inline uint16_t arp_header::get_hardware_type() const { return bswap16(hardware_type); }
	inline uint16_t arp_header::get_proto_type() const { return proto_type; }
	inline uint8_t arp_header::get_hardware_size() const { return hardware_size; }
	inline uint8_t arp_header::et_proto_size() const { return proto_size; }
	inline uint16_t arp_header::get_opcode() const { return opcode; }
	inline mac_address arp_header::get_sender_mac() const { return sender_mac; }
	inline ip_address arp_header::get_sender_ip() const { return sender_ip; }
	inline mac_address arp_header::get_target_mac() const { return target_mac; }
	inline ip_address arp_header::get_target_ip() const { return target_ip; }

	inline std::string arp_header::to_string() const
	{
		std::ostringstream ss;

		ss << "opcode: ";
		switch (opcode)
		{
		case ARP_REQUEST:
			ss << "request";
			break;
		case ARP_REPLY:
			ss << "reply";
			break;
		case RARP_REQUEST:
			ss << "rarp request";
			break;
		case RARP_REPLY:
			ss << "rarp reply";
			break;
		}
		ss << std::endl;

		ss << "sender mac: " << sender_mac << std::endl
			<< "sender ip: " << sender_ip << std::endl
			<< "target mac: " << target_mac << std::endl
			<< "target ip: " << target_ip << std::endl;
		return ss.str();
	}

	inline arp_header& arp_header::operator=(const arp_header& a)
	{
		hardware_type = a.hardware_type;
		proto_type = a.proto_type;
		hardware_size = a.hardware_size;
		proto_size = a.proto_size;
		opcode = a.opcode;
		sender_mac = a.sender_mac;
		sender_ip = a.sender_ip;
		target_mac = a.target_mac;
		target_ip = a.target_ip;
		return *this;
	}

	inline bool arp_header::operator==(const arp_header& a) const
	{
		return hardware_type == a.hardware_type
			&& proto_type == a.proto_type
			&& hardware_size == a.hardware_size
			&& proto_size == a.proto_size
			&& opcode == a.opcode
			&& sender_mac == a.sender_mac
			&& sender_ip == a.sender_ip
			&& target_mac == a.target_mac
			&& target_ip == a.target_ip;
	}

	inline bool arp_header::operator!=(const arp_header& a) const
	{
		return !(*this == a);
	}

	inline std::ostream& operator<<(std::ostream& os, const arp_header& a)
	{
		os << a.to_string();
		return os;
	}

}