#pragma once

namespace noname_core
{
	enum class PacketType : int {
		Ethernet,
		ARP,
		RARP,
		IP,
		TCP,
		UDP,
		HTTP,
		UNKNOWN
	};

}